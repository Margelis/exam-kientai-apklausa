package lt.akademija.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class Survey implements Serializable {

	private static final long serialVersionUID = 4709349123781437446L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private Date survayFillDate;
	@Min(1)
	@Max(10)
	private int companyServiceGrade;
	@Min(1)
	@Max(10)
	private int recomendationGrade;

	@ManyToOne
	private Client client;

	public Survey() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getCompanyServiceGrade() {
		return companyServiceGrade;
	}

	public void setCompanyServiceGrade(int companyServiceGrade) {
		this.companyServiceGrade = companyServiceGrade;
	}

	public int getRecomendationGrade() {
		return recomendationGrade;
	}

	public void setRecomendationGrade(int recomendationGrade) {
		this.recomendationGrade = recomendationGrade;
	}

	public Date getSurvayFillDate() {
		return survayFillDate;
	}

	public void setSurvayFillDate(Date survayFillDate) {
		this.survayFillDate = survayFillDate;
	}

}
