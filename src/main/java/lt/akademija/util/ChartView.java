package lt.akademija.util;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import lt.akademija.controllers.SurveyController;
import lt.akademija.entities.Client;

public class ChartView implements Serializable {

	private static final long serialVersionUID = -8440733608777937235L;

	private BarChartModel barModel;
	private SurveyController surveyController;

	public void init() {
		createBarModels();
	}

	private BarChartModel initBarModel() {
		BarChartModel model = new BarChartModel();
		Map<Client, Double> estimatesMap = surveyController.calculateSurveysEstimates();
		
		Map<Client, Double> sortedMap = new TreeMap<Client, Double>(new Comparator<Client>(){

			@Override
			public int compare(Client o1, Client o2) {//to display from top ID
				return String.valueOf(o1.getId()).compareTo(String.valueOf(o2.getId()));
			}
		} 
		);
		
		sortedMap.putAll(estimatesMap);
		
		ChartSeries user = new ChartSeries();
		user.setLabel("User");
		
		ChartSeries professional = new ChartSeries();
		professional.setLabel("Professional");
		
		ChartSeries expert = new ChartSeries();
		expert.setLabel("Expert");
		
		for(Client c : sortedMap.keySet()){
			double grade = sortedMap.get(c);
			
			if(grade < 7){
				user.set(c.getName() +" "+ c.getLastName(), grade);
			}else if(grade >=7 && grade<8){
				professional.set(c.getName() +" "+ c.getLastName(), grade);
			}else{
				expert.set(c.getName() +" "+ c.getLastName(), grade);
			}
		}

		model.addSeries(user);
		model.addSeries(professional);
		model.addSeries(expert);
		model.setShowPointLabels(true);

		return model;
	}

	private void createBarModels() {
		createBarModel();
	}

	private void createBarModel() {
		barModel = initBarModel();

		barModel.setTitle("Bar Chart");
		barModel.setLegendPosition("ne");

		Axis xAxis = barModel.getAxis(AxisType.X);
		xAxis.setLabel("Cliend");

		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setLabel("Estimates average");
		yAxis.setMin(1);
		yAxis.setMax(10);
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	public SurveyController getSurveyController() {
		return surveyController;
	}

	public void setSurveyController(SurveyController surveyController) {
		this.surveyController = surveyController;
	}
}
