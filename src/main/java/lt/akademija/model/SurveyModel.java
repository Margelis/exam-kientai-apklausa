package lt.akademija.model;

import java.io.Serializable;
import lt.akademija.entities.Survey;

public class SurveyModel implements Serializable {
	
	private static final long serialVersionUID = -2640514586827666915L;
	private Survey currentSurvey;
	
	public Survey getCurrentSurvey() {
		return currentSurvey;
	}
	public void setCurrentSurvey(Survey currentSurvey) {
		this.currentSurvey = currentSurvey;
	}

	
	
}
