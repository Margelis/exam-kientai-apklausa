package lt.akademija.model;

import java.io.Serializable;

import lt.akademija.entities.Client;

public class ClientModel implements Serializable{

	private static final long serialVersionUID = -8563883356667276555L;
	private Client currentClient;

	public Client getCurrentClient() {
		return currentClient;
	}

	public void setCurrentClient(Client currentClient) {
		this.currentClient = currentClient;
	}
}
