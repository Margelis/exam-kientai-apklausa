package lt.akademija.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import lt.akademija.entities.Client;
import lt.akademija.entities.Survey;
import lt.akademija.model.ClientModel;
import lt.akademija.model.SurveyModel;
import lt.akademija.service.ClientService;
import lt.akademija.service.SurveyService;

public class SurveyController {
	private static final Logger log = LogManager.getLogger(SurveyController.class.getName());
	private SurveyModel surveyModel;
	private ClientModel clientModel;
	private SurveyService surveyService;
	private ClientService clientService;

	public void create() {
		surveyModel.setCurrentSurvey(new Survey());
	}

	public void save() {
		Survey survey = surveyModel.getCurrentSurvey();
		survey.setClient(clientModel.getCurrentClient());
		survey.setSurvayFillDate(new Date());

		surveyService.save(survey);
		surveyModel.setCurrentSurvey(null);
		log.info("saved");
	}

	public void cancel() {
		surveyModel.setCurrentSurvey(null);
	}

	public void overview(Survey survey) {
		surveyModel.setCurrentSurvey(survey);
	}

	public List<Survey> findClientSurveys(long id) {
		return surveyService.findClientSurveys(id);
	}

	public void delete(Survey survey) {
		surveyService.delete(survey);
	}

	public Map<Client, Double> calculateSurveysEstimates() {
		List<Client> clientsList = clientService.findAll();

		Map<Client, Double> estimatesMap = new HashMap<>();

		for (Client client : clientsList) {
			List<Survey> surveysList = surveyService.findLastTwoClientSurveys(client.getId());

			double companyServiceGrade = 0;
			double recomendationGrade = 0;

			for (Survey survey : surveysList) {
				companyServiceGrade += survey.getCompanyServiceGrade();
				recomendationGrade += survey.getRecomendationGrade();
			}

			double finalAvg = (companyServiceGrade + recomendationGrade) / 4;

			estimatesMap.put(client, finalAvg);
		}

		return estimatesMap;
	}

	public SurveyModel getSurveyModel() {
		return surveyModel;
	}

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public SurveyService getSurveyService() {
		return surveyService;
	}

	public void setSurveyService(SurveyService surveyService) {
		this.surveyService = surveyService;
	}

	public void setSurveyModel(SurveyModel surveyModel) {
		this.surveyModel = surveyModel;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}
}
