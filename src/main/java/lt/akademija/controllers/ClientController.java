package lt.akademija.controllers;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import lt.akademija.entities.Client;
import lt.akademija.model.ClientModel;
import lt.akademija.service.ClientService;

public class ClientController {
	
	private ClientModel clientModel;
	private ClientService clientService;
	
	public void create(){
		clientModel.setCurrentClient(new Client());
	}
	
	public void save(){
		
		if(clientService.checkIfUserCanBeRegistered(clientModel.getCurrentClient())){
			clientService.save(clientModel.getCurrentClient());
			clientModel.setCurrentClient(null);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('addClient').hide();");
			
		}else{
			 FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Client already exist"));
		}
	}
	
	public void cancel(){
		clientModel.setCurrentClient(null);
	}
	
	public void overview(Client client){
		clientModel.setCurrentClient(client);
	}
	
	public List<Client> findClients(){
		return clientService.findAll();
	}
	
	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}
}
