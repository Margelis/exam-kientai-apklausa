package lt.akademija.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import lt.akademija.entities.Client;

public class ClientDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}
	
	public void save(Client newClient) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(newClient))
			newClient = em.merge(newClient);
		em.persist(newClient);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Client client) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		client = em.merge(client);
		em.remove(client);
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Client> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Client> cq = cb.createQuery(Client.class);
			Root<Client> root = cq.from(Client.class);
			cq.select(root); // we select entity here
			TypedQuery<Client> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public boolean checkIfUserCanBeRegistered(Client client) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Client> clientQuery = em.createQuery("SELECT p From Client p WHERE p.name = :name AND p.lastName = :lastName AND p.birthday = :birthday", Client.class);
		clientQuery.setParameter("name", client.getName());
		clientQuery.setParameter("lastName", client.getLastName());
		clientQuery.setParameter("birthday", client.getBirthday());
		return clientQuery.getResultList().isEmpty();
	}
	
	public Client findById(Integer id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Client> clientQuery = em.createQuery("SELECT p From Client p WHERE p.id = :id", Client.class);
		clientQuery.setParameter("id", id);
		clientQuery.setMaxResults(1);
		return clientQuery.getSingleResult();

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
}
