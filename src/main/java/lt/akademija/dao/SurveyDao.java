package lt.akademija.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import lt.akademija.entities.Survey;

public class SurveyDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public void save(Survey survey) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(survey))
			survey = em.merge(survey);
		em.persist(survey);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Survey survey) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		survey = em.merge(survey);
		em.remove(survey);
		em.getTransaction().commit();
		em.close();
	}

	public List<Survey> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Survey> cq = cb.createQuery(Survey.class);
			Root<Survey> root = cq.from(Survey.class);
			cq.select(root); // we select entity here
			TypedQuery<Survey> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public List<Survey> findLastTwoClientSurveys(long id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Survey> clientQuery = em.createQuery(
				"SELECT p From Survey p WHERE p.client.id = :id ORDER BY survayFillDate DESC", Survey.class);
		clientQuery.setParameter("id", id);
		clientQuery.setMaxResults(2);
		return clientQuery.getResultList();
	}

	public List<Survey> findClientSurveys(long id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Survey> clientQuery = em.createQuery("SELECT p From Survey p WHERE p.client.id = :id", Survey.class);
		clientQuery.setParameter("id", id);

		return clientQuery.getResultList();
	}

	public Survey findById(long id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Survey> clientQuery = em.createQuery("SELECT p From Survey p WHERE p.id = :id", Survey.class);
		clientQuery.setParameter("id", id);
		clientQuery.setMaxResults(1);
		return clientQuery.getSingleResult();

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
}
