package lt.akademija.service;

import java.util.List;

import lt.akademija.dao.SurveyDao;
import lt.akademija.entities.Survey;

public class SurveyService {
	private SurveyDao surveyDao;

	public void save(Survey survey) {
		surveyDao.save(survey);
	}

	public void delete(Survey survey) {
		surveyDao.delete(survey);
	}

	public List<Survey> findAll() {
		return surveyDao.findAll();
	}

	public List<Survey> findLastTwoClientSurveys(long id) {
		return surveyDao.findLastTwoClientSurveys(id);
	}

	public Survey findById(long id) {
		return surveyDao.findById(id);
	}

	public List<Survey> findClientSurveys(long id) {
		return surveyDao.findClientSurveys(id);
	}

	public SurveyDao getSurveyDao() {
		return surveyDao;
	}

	public void setSurveyDao(SurveyDao surveyDao) {
		this.surveyDao = surveyDao;
	}

}
